-- la BD est créée automatiquement lorsque vous lancez l'application
-- la BD sera stocker dans le repertoir «db/db.db»

CREATE TABLE IF NOT EXISTS article
 (id integer primary key,
   titre varchar(100),
   identifiant varchar(50),
   auteur varchar(100),
   date_publication text,
   paragraphe varchar(500)
);

CREATE TABLE IF NOT EXISTS utilisateurs (
  id integer primary key,
  utilisateur varchar(25),
  email varchar(100),
  salt varchar(32),
  hash varchar(128)
);

CREATE TABLE IF NOT EXISTS sessions (
  id integer primary key,
  id_session varchar(32),
  utilisateur varchar(25)
);

CREATE TABLE IF NOT EXISTS invites (
  id integer primary key,
  email varchar(100),
  jeton varchar(128)
);
