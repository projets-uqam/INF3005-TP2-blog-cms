API-Documentation
=================

Application Programming Interface for the following services:

1. Create an Article
2. Get data from a specific article
3. Get the list of articles already published

Type of data returned: JSON


Feature #1: Create an Article
-------------------------------

* **URL**

  /api/creer-un-article/

* **Method:**

  `POST`

*  **URL Params**

   **Not Required:**

* **Payload HTTP Body**
  - exemple des données à fournir lors de l’appel

```
 {
  "titre": "Article #3",
  "identifiant": "article-3",
  "auteur": "Alex Gomez",
  "date_publication": "2016-11-09",
  "paragraphe": "le texte...article3"
  }
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `None`

* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** L'identifiant pour cette article est déjà utilisé


Feature #2: get data from a specific article
---------------------------------------------

* **URL**

  /api/articles/

* **Method:**

  `GET`

*  **URL Params**

   **Not Required:**

* **Payload HTTP Body**

  **Not Required:**

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** la liste des articles déjà publiés

* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** La liste d’articles demandée n’existe pas ou n’existe plus


Feature #3: get the list of articles already published
-------------------------------------------------------

* **URL**

  /api/article/<_identifiant_>

* **Method:**

  `GET`

*  **URL Params**

   **Required:** `<identifiant>`
    **exemple:** /api/article/article-102

*  **Payload HTTP Body**

   **Not Required:**

* **Success Response:**

  * **Code:** 200 <br />
  * **Content:** article demandée

* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
  * **Content:** L’article demandée n’existe pas ou n’existe plus
