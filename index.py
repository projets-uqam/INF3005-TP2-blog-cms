# coding: utf8
# -*- coding: utf-8 -*-
# Copyright 2017 Alexander Acevedo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask
from flask import render_template
from flask import g
from datetime import datetime
from flask import redirect
from flask import request
from database import Database
from flask import jsonify
import unicodedata
import re
import json
import hashlib
import uuid
from flask import session
from functools import wraps
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText


app = Flask(__name__)

# -------- Méthodes utilitaires ------------------------


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


def batir_init_formulaire():
    return {"titre": "",
            "identifiant": "",
            "auteur": "",
            "date_publication": "",
            "paragraphe": ""
            }


def valider_format_date(date_publication):
    try:
        if date_publication != datetime.strptime(date_publication, "%Y-%m-%d").strftime('%Y-%m-%d'):
            raise ValueError
        return True
    except ValueError:
        return False


def creer_user_correcteur():
    username, password = "correcteur", "secret"
    email = "correcteur2626@gmail.com"
    salt = uuid.uuid4().hex
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    db = get_db()
    db.creer_utilisateur(username, email, salt, hashed_password)


def determiner_username():
    username = None
    if "id" in session:
        username = get_db().get_session(session["id"])
    return username


def authentication_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated(session):
            return send_unauthorized()
        return f(*args, **kwargs)
    return decorated


def is_authenticated(session):
    return "id" in session


def send_unauthorized():
    return render_template('401.html'), 401


def lire_data_source_courriel():
    return json.loads(open('source_courriel.json').read())


def generer_jeton(courriel):
    return hashlib.sha512(courriel).hexdigest()


def generer_identifiant(le_titre):
    url = unicodedata.normalize('NFKD', le_titre).encode('ascii', 'ignore')
    return re.sub(r'\W+', '-', url).strip('-').lower()


# ----- ROUTES -------------------------------------------
@app.route('/')
def afficher_accueil():
    articles = get_db().obtenir_articles_5()
    return render_template('accueil.html', articles=articles)


@app.route('/admin')
def afficher_admin():
    user_correcteur = get_db().get_utilisateur_info("correcteur")
    if user_correcteur is None:
        creer_user_correcteur()
    return render_template('admin.html')


@app.route('/admin-panneau')
@authentication_required
def afficher_admin_panneau():
    articles = get_db().obtenir_articles()
    articles_a_afficher = get_db().obtenir_articles_a_afficher()
    username = determiner_username()
    return render_template(
            'admin_panneau.html',
            articles=articles,
            articles_a_afficher=articles_a_afficher,
            username=username)


@app.route('/admin-nouveau')
@authentication_required
def afficher_formulaire():
    init_formulaire = batir_init_formulaire()
    erreurs = {}
    username = determiner_username()
    return render_template(
            'nouvel_article.html',
            init_formulaire=init_formulaire,
            erreurs=erreurs, username=username)


@app.route('/formulaire', methods=['POST'])
def capturer_donnees_article():
    username = determiner_username()
    init_formulaire = batir_init_formulaire()
    erreurs = {}
    le_titre = request.form['titre'].strip()
    l_identifiant = generer_identifiant(le_titre)
    identifiant_unique = get_db().verif_identifiant_unique(l_identifiant)
    l_auteur = request.form['auteur'].strip()
    la_date_publication = request.form['date_publication'].strip()
    le_paragraphe = request.form['paragraphe'].strip()
    # validation du formulaire
    if len(le_titre) == 0:
        erreurs["titre"] = "Le titre est obligatoire"
    else:
        init_formulaire["titre"] = le_titre
    if identifiant_unique is not None:
        erreurs["titre"] = "L'identifiant n'est pas unique. " \
                            "SVP changez votre Titre !"
    if len(l_auteur) == 0:
        erreurs["auteur"] = "L'auteur est obligatoire"
    else:
        init_formulaire["auteur"] = l_auteur
    if len(la_date_publication) == 0:
        erreurs["date_publication"] = "La date de publication est obligatoire"
    else:
        if valider_format_date(la_date_publication) is False:
            erreurs["date_publication_format"] = "Le format de la date que " \
                                        "vous avez introduit est incorrect"
            init_formulaire["date_publication"] = la_date_publication
        else:
            init_formulaire["date_publication"] = la_date_publication
    if len(le_paragraphe) == 0:
        erreurs["paragraphe"] = "Le paragraphe est obligatoire"
    else:
        init_formulaire["paragraphe"] = le_paragraphe
    if erreurs.values():
        return render_template(
                'nouvel_article.html',
                init_formulaire=init_formulaire,
                erreurs=erreurs,
                username=username)
    else:
        get_db().ajouter_article(le_titre, l_identifiant,
                                 l_auteur, la_date_publication,
                                 le_paragraphe)
        return redirect('/admin-panneau')


@app.route('/formulaire_modifie/<identifiant>', methods=['POST'])
def capturer_donnees_modifie(identifiant):
    username = determiner_username()
    article = get_db().obtenir_article(identifiant)
    titre = article["titre"]
    le_titre = request.form['titre'].strip()
    l_identifiant = generer_identifiant(le_titre)
    identifiant_unique = get_db().verif_identifiant_unique(l_identifiant)
    le_paragraphe = request.form['paragraphe'].strip()
    if len(le_titre) == 0 or len(le_paragraphe) == 0:
        erreur_modification = "Attention ! Tous les champs sont " \
                            "obligatoires pendant la modification"
        return render_template('modifier.html',
                               article=article,
                               erreur_modification=erreur_modification,
                               username=username)
    elif identifiant_unique is not None and le_titre != titre:
        erreur_modification = "Attention, l'identifiant n'est pas unique. " \
                                "SVP changez votre Titre !"
        return render_template('modifier.html',
                               article=article,
                               erreur_modification=erreur_modification,
                               username=username)
    else:
        get_db().modifier_article(le_titre,
                                  l_identifiant,
                                  identifiant,
                                  le_paragraphe)
        return redirect('/admin-panneau')


@app.route('/article/<identifiant>')
def afficher_article(identifiant):
    article = get_db().obtenir_article(identifiant)
    if article is None:
        return render_template('404.html'), 404
    else:
        return render_template('article.html', article=article)


@app.route('/modifier_article/<identifiant>')
@authentication_required
def afficher_article_modifie(identifiant):
    username = determiner_username()
    article = get_db().obtenir_article(identifiant)
    if article is None:
        return render_template('404.html', username=username), 404
    else:
        return render_template('modifier.html',
                               username=username,
                               article=article)


@app.route('/recherche', methods=['POST'])
def afficher_resultat_recherche():
    la_recherche = request.form['recherche']
    resultat_recherche = get_db().obtenir_recherche(la_recherche)
    username = determiner_username()
    if username is None:
        return render_template('recherche.html',
                               resultat_recherche=resultat_recherche,
                               la_recherche=la_recherche)
    else:
        return render_template('admin_recherche.html',
                               resultat_recherche=resultat_recherche,
                               la_recherche=la_recherche,
                               username=username)


@app.route('/invitation')
@authentication_required
def afficher_formulaire_invitation():
    username = determiner_username()
    return render_template('invitation.html', username=username)


@app.route('/start-recouvrement')
def afficher_formulaire_recouvrement():
    return render_template('start_recouvrement.html')


@app.route('/envoyer-recouvrement', methods=["POST"])
def envoyer_recouvrement():
    source_address = lire_data_source_courriel()["source_address"]
    destination_address = request.form["courriel"]
    if len(destination_address) == 0:
        erreur = "Le courriel est obligatoire"
        return render_template('start_recouvrement.html', erreur=erreur)
    exist = get_db().verif_email_unique(destination_address)
    if exist is None:
        erreur = "Adresse courriel inexistant, svp essayez de nouveau"
        return render_template('start_recouvrement.html', erreur=erreur)
    else:
        jeton = generer_jeton(destination_address)
        subject = "Recouvrement du Mot de Passe au CMS «MON-BLOG»"
        body = 'Bonjour!\nVeuillez cliquer sur le lien ci-dessous ' \
               'afin récuperer votre mot de passe\n' \
               'http://localhost:5000/recouvrement-link/' + jeton
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = source_address
        msg['To'] = destination_address
        msg.attach(MIMEText(body, 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(source_address,
                     lire_data_source_courriel()["source_address_psw"])
        text = msg.as_string()
        server.sendmail(source_address, destination_address, text)
        server.quit()
        return redirect('/')


@app.route('/envoyer-invitation', methods=["POST"])
def envoyer_invitation():
    username = determiner_username()
    source_address = lire_data_source_courriel()["source_address"]
    destination_address = request.form["courriel"]
    if len(destination_address) == 0:
        erreur_courriel_vide = "Le courriel est obligatoire"
        return render_template('invitation.html',
                               erreur_courriel_vide=erreur_courriel_vide,
                               username=username)
    else:
        jeton = generer_jeton(destination_address)
        get_db().creer_invite(destination_address, jeton)
        subject = "Invitation pour avoir accès au CMS «MON-BLOG»"
        body = 'Bonjour!\nVeuillez cliquer sur le lien ci-dessous ' \
               'afin de devenir un Utilisateur\n' \
               'http://localhost:5000/invitation-link/' + jeton
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = source_address
        msg['To'] = destination_address
        msg.attach(MIMEText(body, 'plain'))
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(source_address,
                     lire_data_source_courriel()["source_address_psw"])
        text = msg.as_string()
        server.sendmail(source_address, destination_address, text)
        server.quit()
        return redirect('/admin-panneau')


@app.route('/invitation-link/<jeton>')
def verifier_jeton_invitation(jeton):
    email = get_db().get_courriel_invite(jeton)
    email = str(email).strip("(u',)")
    if email is None:
        return render_template('404.html'), 404
    else:
        return render_template('registration.html', email=email)


@app.route('/recouvrement-link/<jeton>')
def verifier_jeton_recouvrement(jeton):
    email = get_db().get_courriel_invite(jeton)
    email = str(email).strip("(u',)")
    username = get_db().get_utilisateur(email)
    username = str(username).strip("(u',)")
    if email is None:
        return render_template('404.html'), 404
    else:
        return render_template('recouvrement.html',
                               email=email,
                               username=username)


@app.route('/confirmation')
def confirmation_page():
    return render_template('login_confirmation.html')


@app.route('/confirmation-recouvrement')
def confirmation_recouvrement_page():
    return render_template('confirmation_recouvrement.html')


@app.route('/registration', methods=["POST"])
def formulaire_registration():
    username = request.form["utilisateur"]
    password = request.form["mot_passe"]
    email = request.form["email"]
    if username == "" or password == "" or email == "":
        erreur = "Tous les champs sont obligatoires."
        return render_template('registration.html', erreur=erreur)
    exist = get_db().verif_utilisateur_unique(username)
    if exist is not None:
        erreur = "Ce Nom d'Utilisateur existe."
        return render_template('registration.html', erreur=erreur)
    salt = uuid.uuid4().hex
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    get_db().creer_utilisateur(username, email, salt, hashed_password)
    return redirect("/confirmation")


@app.route('/formulaire-recouvrement', methods=["POST"])
def formulaire_recouvrement():
    username = request.form["utilisateur"]
    password = request.form["mot_passe"]
    email = request.form["email"]
    if password == "":
        erreur = "Tous les champs sont obligatoires."
        return render_template('recouvrement.html',
                               erreur=erreur,
                               email=email,
                               username=username)
    salt = uuid.uuid4().hex
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    get_db().updating_mot_passe(username, salt, hashed_password)
    return redirect("/confirmation-recouvrement")


@app.route('/login', methods=["POST"])
def log_user():
    username = request.form["username"]
    password = request.form["password"]
    if username == "" or password == "":
        erreur_login = "Tous les champs sont obligatoires " \
                        "pendant l'authentification"
        return render_template('admin.html', erreur_login=erreur_login)
    user = get_db().get_utilisateur_info(username)
    if user is None:
        erreur_login = "Erreur avec le Nom d'Utilisateur ou le Mot de Passe"
        return render_template('admin.html', erreur_login=erreur_login)
    user = get_db().get_utilisateur_info(username)
    salt = user[0]
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    if hashed_password == user[1]:
        id_session = uuid.uuid4().hex
        get_db().save_session(id_session, username)
        session["id"] = id_session
        return redirect("/admin-panneau")
    else:
        erreur_login = "Erreur avec le Nom d'Utilisateur ou le Mot de Passe"
        return render_template('admin.html', erreur_login=erreur_login)


@app.route('/logout')
@authentication_required
def logout():
    if "id" in session:
        id_session = session["id"]
        session.pop('id', None)
        get_db().delete_session(id_session)
    return redirect("/")


# -------- API -------------------------------------------------------
# API --> Feature #1: create an Article
@app.route('/api/creer-un-article/', methods=["POST"])
def creer_article():
    data = request.get_json(force=True)  # input from Advanced Rest Client
    unique = get_db().verif_identifiant_unique(data["identifiant"])
    if unique is None:
        get_db().ajouter_article(data["titre"],
                                 data["identifiant"],
                                 data["auteur"],
                                 data["date_publication"],
                                 data["paragraphe"])
        return "", 200
    else:
        return "L'identifiant pour cette article est déjà utilisé", 400


# API --> Feature #2: get data from a specific article
@app.route('/api/article/<identifiant>')
def data_article(identifiant):
    article = get_db().obtenir_article(identifiant)
    if article is not None:
        return jsonify(article)
    else:
        return "L'article demandée n’existe pas ou n’existe plus", 400


# API --> Feature #3: get the list of articles already published
@app.route('/api/articles/', methods=["GET"])
def liste_articles():
    articles = get_db().obtenir_articles()
    data = [{"titre": each["titre"],
             "l'auteur": each["auteur"],
             "url": each["identifiant"]} for each in articles]
    if len(data) != 0:
        return jsonify(data)
    else:
        return "La liste d'article demandée n’existe pas ou n’existe plus", 400


app.secret_key = "(*&*&322387he738220)(*(*22347657"


# Running the Application
if __name__ == "__main__":
    app.run()
