# coding: utf8
# -*- coding: utf-8 -*-
# Copyright 2017 Alexander Acevedo
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sqlite3
from os.path import exists


def batir_article_dictionary(row):
    return {"titre": row[0],
            "identifiant": row[1],
            "auteur": row[2],
            "date_publication": row[3],
            "paragraphe": row[4]}


def lire_script_sql():
    scriptSql = open('db/db.sql', 'r').read()
    return scriptSql


class Database:
    def __init__(self):
        self.connection = None

    # creation de la BD
    def create_db(self):
        self.connection = sqlite3.connect('db/db.db')
        self.connection.executescript(lire_script_sql())

    # connexion à la BD
    def get_connection(self):
        if exists('db/db.db'):
            if self.connection is None:
                self.connection = sqlite3.connect('db/db.db')
        else:
            self.create_db()
            self.connection = sqlite3.connect('db/db.db')
        return self.connection

    # déconnexion à la BD
    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    # manipulation des articles
    def ajouter_article(self, le_titre, l_identifiant, l_auteur,
                        la_date_publication, le_paragraphe):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute(("insert into article(titre, identifiant, auteur, date_publication, paragraphe)"
                        "values(?, ?, ?, ?, ?)"), (le_titre, l_identifiant, l_auteur, la_date_publication, le_paragraphe))
        connection.commit()

    def verif_identifiant_unique(self, identifiant):
        cursor = self.get_connection().cursor()
        cursor.execute(("select CASE WHEN EXISTS (select * from article where identifiant =?)THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END"),
                       (identifiant,))
        reponse = cursor.fetchone()
        reponse = str(reponse).strip("(u',)")
        if reponse is '0':
            return None
        else:
            return reponse

    def modifier_article(self, le_titre, identifiant_new,
                         identifiant_old, le_paragraphe):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute(("UPDATE article SET titre= ?, paragraphe= ?, identifiant= ? WHERE identifiant= ?"), (le_titre, le_paragraphe, identifiant_new, identifiant_old,))
        connection.commit()

    def obtenir_all_articles(self):
        cursor = self.get_connection().cursor()
        cursor.execute("select titre, identifiant, auteur, date_publication, paragraphe from article")
        articles = cursor.fetchall()
        return [batir_article_dictionary(each) for each in articles]

    def obtenir_articles(self):
        cursor = self.get_connection().cursor()
        cursor.execute("select titre, identifiant, auteur, date_publication, paragraphe from article where date(date_publication) <= date('now')")
        articles = cursor.fetchall()
        return [batir_article_dictionary(each) for each in articles]

    def obtenir_articles_a_afficher(self):
        cursor = self.get_connection().cursor()
        cursor.execute("select titre, identifiant, auteur, date_publication, paragraphe from article where date(date_publication) > date('now')")
        articles = cursor.fetchall()
        return [batir_article_dictionary(each) for each in articles]

    def obtenir_articles_5(self):
        cursor = self.get_connection().cursor()
        cursor.execute("select titre, identifiant, auteur, date_publication, paragraphe from article where date(date_publication) <= date('now') order by date(date_publication) desc limit 5")
        articles = cursor.fetchall()
        return [batir_article_dictionary(each) for each in articles]

    def obtenir_recherche(self, recherche):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT titre, identifiant, auteur, date_publication, paragraphe FROM article WHERE titre like ? or identifiant like ? or paragraphe like ?"), ('%%%s%%' % recherche, '%%%s%%' % recherche, '%%%s%%' % recherche, ))
        articles = cursor.fetchall()
        return [batir_article_dictionary(each) for each in articles]

    def obtenir_article(self, identifiant):
        cursor = self.get_connection().cursor()
        cursor.execute("select titre, identifiant, auteur, date_publication, paragraphe from article where identifiant = ?", (identifiant,))
        article = cursor.fetchone()
        if article is None:
            return None
        else:
            return batir_article_dictionary(article)

    # manipulation des utilisateurs
    def get_utilisateur(self, email):
        cursor = self.get_connection().cursor()
        cursor.execute(("select utilisateur from utilisateurs where email=?"),
                       (email,))
        utilisateur = cursor.fetchone()
        return utilisateur

    def get_utilisateurs(self):
        cursor = self.get_connection().cursor()
        cursor.execute("select id, utilisateur from utilisateurs")
        utilisateurs = cursor.fetchall()
        return [(un_utilisateur[0], un_utilisateur[1]) for un_utilisateur in utilisateurs]

    def creer_utilisateur(self, username, email, salt, hashed_password):
        connection = self.get_connection()
        connection.execute(("insert into utilisateurs (utilisateur, email, salt, hash)"
                            " values(?, ?, ?, ?)"), (username, email, salt, hashed_password))
        connection.commit()

    def updating_mot_passe(self, username, salt, hashed_password):
        connection = self.get_connection()
        connection.execute(("update utilisateurs set salt = ? where utilisateur=?"),
                           (salt, username,))
        connection.execute(("update utilisateurs set hash = ? where utilisateur=?"),
                           (hashed_password, username,))
        connection.commit()

    def get_utilisateur_info(self, username):
        cursor = self.get_connection().cursor()
        cursor.execute(("select salt, hash from utilisateurs where utilisateur=?"),
                       (username,))
        user = cursor.fetchone()
        if user is None:
            return None
        else:
            return user[0], user[1]

    def verif_utilisateur_unique(self, username):
        cursor = self.get_connection().cursor()
        cursor.execute(("select count (id) from utilisateurs where utilisateur=?"),
                       (username,))
        reponse = cursor.fetchone()
        reponse = str(reponse).strip("(u',)")
        if reponse is '0':
            return None
        else:
            return reponse

    def verif_email_unique(self, email):
        cursor = self.get_connection().cursor()
        cursor.execute(("select CASE WHEN EXISTS (select * from utilisateurs where email =?)THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END"),
                       (email,))
        reponse = cursor.fetchone()
        reponse = str(reponse).strip("(u',)")
        if reponse is '0':
            return None
        else:
            return reponse

    # manipulation des sessions
    def save_session(self, id_session, username):
        connection = self.get_connection()
        connection.execute(("insert into sessions(id_session, utilisateur) "
                            "values(?, ?)"), (id_session, username))
        connection.commit()

    def get_session(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("select utilisateur from sessions where id_session=?"),
                       (id_session,))
        data = cursor.fetchone()
        if data is None:
            return None
        else:
            return data[0]

    def delete_session(self, id_session):
        connection = self.get_connection()
        connection.execute(("delete from sessions where id_session=?"),
                           (id_session,))
        connection.commit()

    # manipulation des invites
    def creer_invite(self, le_courriel, le_jeton):
        connection = self.get_connection()
        connection.execute(("insert into invites (email, jeton)"
                            " values(?, ?)"), (le_courriel, le_jeton))
        connection.commit()

    def get_courriel_invite(self, jeton):
        cursor = self.get_connection().cursor()
        cursor.execute(("select email from invites where jeton=?"),
                       (jeton,))
        email = cursor.fetchone()
        if email is None:
            return None
        else:
            return email
