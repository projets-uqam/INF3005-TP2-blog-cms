BLOG-CMS
===================================================
TP2 - H2017 - INF3005 - Programmation Web Avancée

Summary
---------
* Building a simple BLOG-CMS with Python/Flask from scratch


Functions
---------

* Create and manage articles
* The admin can add contributors
* Multiple authors can be attributed to blog posts


Technologies used:
------------------
* HTML 5
* CSS 3
* Python 2.7
* Flask
* Jinja2
* SQlite3


Running the Application
---------------------------

```sh
$ python index.py
```


Database
---------
* script to create the database [SQlite](db/db.sql)
* the database is created automatically when you launch the application if not exist
* the database will be stored in the «db» directory


Super Admin
------------
* the Super Admin may access services related to the administration
    * nom d'utilisation: 'correcteur'
    * mot de passe: 'secret'
    * route: /admin


Email Configuration File «.json»
-------------------------------------
* file name: source_courriel.json
* purpose: customize Gmail address
* content:
    * source_address: gmail username
    * source_address_psw: password


API - Documentation
--------------------
* file name: README_API.md
* purpose: documentation for API services


Password Recovery Tool
-----------------------------------------------------
* route: / admin
* route: /start-recouvrement


Next
-----
* use the modules
* use the makefile
* reduce the number of methods lines  


Reference
----------
* INF3005 | Hiver 2017 | Professeur: Jacques Berger
* [udemy-course](https://www.udemy.com/php-y-mysql/)


License
--------
This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
